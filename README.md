# Device Finder

**Device Finder** uses [SuperFlyTV's xkeys module](https://github.com/SuperFlyTV/xkeys) to mimic [P.I.Engineering's](https://xkeys.com) own [Device Finder](https://xkeys.com/software/softwareutilities.html) utility.

After startup (see below), any X-Keys devices currently attached to the machine will be listed in the upper section of the gui. 

Selecting any one of the devices from the list will show information gathered from the device in the _Device info_ display area. Additionally, pressing the _Device info_ button will identify the selected device by flashing its red LED for three seconds.

Finally, after pressing the _Read Device_ button, any button pressing, knob twiddling, etc., of the selected device will show relevant information about such events in the _Read device_ display area (which may be cleared by pressing the _Clear_ button).

If additional devices are attached while **Device Finder** is running, they will be automatically detected and listed.


# Status

In the main branch here, the [Electron](https://www.electronjs.org/) framework is used to combine JavaScript, HTML and CSS. Other frameworks used have included [node-gui](https://docs.nodegui.org/) and [NWjs](https://github.com/nwjs/nw.js) which may be accessed from the `use-nodegui` and `use-nwjs` branches respectively. However future development (beyond version 1.0.0) is now restricted to the Electron framework.

Tested on Linux and Raspberry Pi OS (buster) but I believe all modules used are cross platform so should work on Windows & Mac too.

I have no devices with joystick or T-bar available for testing. I think the code for them is correct ...

Any reports of success or failure welcome via [Issues page.](https://gitlab.com/chris.willing/devicefinder/-/issues)


# Installation

The main preinstallation requirements are recent versions of `git` and `nodejs`. Installation on a Raspberry Pi also requires `libudev-dev`.

Then, download this repository with:

`git clone https://gitlab.com/chris.willing/devicefinder.git`

Enter the resulting _devicefinder_ directory

`cd devicefinder`

 and run:

`npm install`

which will download and install the required javascript/typescript modules. To build and run **Device Finder** itself, now run:

`npm start`

The result, after pressing a few buttons, should look something like:

<img src="/assets/dfrunning.png"  width="475" height="680">


# Distribution

The main (Electron) branch includes [electron-builder](https://www.electron.build/) to enable creation of standalone executables. Run:

`npm run dist`

to create an executable for the current platform (only Linux able to be tested so far), which will create a `dist` directory in which will be found the resulting _DeviceFinder-1.0.0.AppImage_ executable. This can be renamed to whatever seems appropriate (_devicefinder_?) and installed in the local PATH so that it may be run like any other application.

# Issues

On Linux, if no connected devices are detected, it's likely that there is a permissions issue which, as mentioned at [SuperFlyTV's xkeys project site](https://github.com/SuperFlyTV/xkeys), can be fixed by installing the appropriate configuration file as quoted here:

>Save the following to `/etc/udev/rules.d/50-xkeys.rules` and reload the rules with `sudo udevadm control --reload-rules && sudo udevadm trigger`
>
>```
>SUBSYSTEM=="input", GROUP="input", MODE="0666"
>SUBSYSTEM=="usb", ATTRS{idVendor}=="05f3", MODE:="0666", GROUP="plugdev"
>KERNEL=="hidraw*", ATTRS{idVendor}=="05f3", MODE="0666", GROUP="plugdev"
>```

# Also

[SuperFlyTV's xkeys project site](https://github.com/SuperFlyTV/xkeys) now has an online demo app similar to DeviceFinder. [Try the Demo](https://superflytv.github.io/xkeys/) (requires Chromium based browser).

