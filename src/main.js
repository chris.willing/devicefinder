const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const { XKeys, XKeysWatcher } = require('xkeys');

let watcher;

// Container of XKeys devices indexed by path
let xkeys_devices = {};

function createWindow () {
	const win = new BrowserWindow({
		width: 567,
		height: 800,
		title: 'Device Finder',
		autoHideMenuBar: true,
		icon: path.join(__dirname, '../assets/pielogox200.png'),
		webPreferences: {
			preload: path.join(__dirname, 'index.js')
		}
	})
	win.loadFile('src/index.html');

	win.webContents.on('did-finish-load', () => {
		watcher = new XKeysWatcher();
		watcher.on('connected', (xkeysPanel) => {
			//console.log(`X-keys panel ${xkeysPanel.devicePath} connected`)
			xkeys_devices[xkeysPanel.devicePath] = xkeysPanel;
			win.webContents.send('newDevice', xkeysPanel.devicePath);

			xkeysPanel.on('disconnected', () => {
				//console.log(`X-keys panel ${xkeysPanel.devicePath} was disconnected`)
				if (!win.isDestroyed()) {
					win.webContents.send("lostDevice", xkeysPanel.devicePath);
				}
				// Clean up stuff
				delete xkeys_devices[xkeysPanel.devicePath];
				xkeysPanel.removeAllListeners();
				xkeysPanel.close();
			})
			xkeysPanel.on('down', (btnIndex, metadata) => {
				//console.log('Button pressed', btnIndex, metadata);
				win.webContents.send('deviceEvent', [xkeysPanel.devicePath,
					`button ${btnIndex} down, row ${metadata['row']}, col ${metadata['col']}, timestamp ${metadata['timestamp']}`
				]);
			})
			xkeysPanel.on('up', (btnIndex, metadata) => {
				//console.log('Button released', btnIndex, metadata);
				win.webContents.send('deviceEvent', [xkeysPanel.devicePath,
					`button ${btnIndex} up, row ${metadata['row']}, col ${metadata['col']}, timestamp ${metadata['timestamp']}`
				]);
			})
			xkeysPanel.on('jog', (index, deltaPos, metadata) => {
				//console.log(`Jog ${index} position has changed`, deltaPos, metadata)
				win.webContents.send('deviceEvent', [xkeysPanel.devicePath,
					`jog ${index}, delta ${deltaPos}, timestamp ${metadata['timestamp']}`
				]);
			})
			xkeysPanel.on('shuttle', (index, shuttlePos, metadata) => {
				//console.log(`Shuttle ${index} position has changed`, shuttlePos, metadata)
				win.webContents.send('deviceEvent', [xkeysPanel.devicePath,
					`shuttle ${index}, pos ${shuttlePos}, timestamp ${metadata['timestamp']}`
				]);
			})
			xkeysPanel.on('joystick', (index, position, metadata) => {
				//console.log(`Joystick ${index} position has changed`, position, metadata) // {x, y, z}
				win.webContents.send('deviceEvent', [xkeysPanel.devicePath,
					`joystick ${index}, xPos ${position['x']}, yPos ${position['y']}, twist ${position['deltaZ']}, timestamp ${metadata['timestamp']}`
				]);
			})
			xkeysPanel.on('tbar', (index, position, metadata) => {
				//console.log(`T-bar ${index} position has changed`, position, metadata)
				win.webContents.send('deviceEvent', [xkeysPanel.devicePath,
					`tbar ${index}, pos ${position}, timestamp ${metadata['timestamp']}`
				]);
			})
			xkeysPanel.on('error', (...errs) => {
				console.log('X-keys watcher error:', ...errs)
			})
		})
	});

}

app.whenReady().then(() => {
	let win = null;

	createWindow();

	app.on('activate', () => {
		if (BrowserWindow.getAllWindows().length === 0) {
			createWindow()
		}
	})
})

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		watcher.stop()
		.then(() => {
			ipcMain.removeAllListeners();
			app.quit()
		})
		.catch(console.error)
	}
})


// Messages from browser context
ipcMain.on('deviceList', (event) => {
	// Return abbreviated list {path:name} pairs
	//console.log("device list requested");
	let device_list = {};
	for (const key of Object.keys(xkeys_devices) ) {
		//console.log("Found ", xkeys_devices[key].info.name, " at ", key);
		device_list[key] = xkeys_devices[key].info.name;
	}
	event.returnValue = device_list;
});

ipcMain.on('deviceInfo', (event, target) => {
	//console.log("device info requested for ", target);
	event.returnValue = xkeys_devices[target].info;
});

ipcMain.on('identify', (event, target) => {
	//console.log("identification requested for ", target);
	xkeys_devices[target].setFrequency(20);
	xkeys_devices[target].setIndicatorLED(2, true, true)
	setTimeout((device) => {
		device.setIndicatorLED(2, false);
	}, 3000, xkeys_devices[target]);
});

