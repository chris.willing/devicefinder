const { ipcRenderer } = require("electron");

window.addEventListener('DOMContentLoaded', () => {

	// "Find devices" button
	var findDevices = document.getElementById('findDevicesButton');
	if (!findDevices) {
		alert("No pushbutton");
		return;
	}
	findDevices.onclick = function () {
		// Clear everything
		clear_deviceList();
		clear_infoArea();
		clear_dataArea();

		///////xkdev.connect_devices();
	}
	ipcRenderer.on('lostDevice', function (event, target) {
		// We need to remove deviceList entry;
		// also infoArea & dataArea if the lost Device had been selected

		let targetChecked = false;
		let deviceListHolder = document.getElementById('deviceListHolder');
		let deviceListElements =  deviceListHolder.children;
		for (let i=deviceListElements.length-1;i>-1;i--) {
			if (deviceListElements[i].hasAttribute('source') && deviceListElements[i].getAttribute('source') == target ) {
				if (deviceListElements[i].checked ) {
					targetChecked = true;
				}
				deviceListHolder.removeChild(deviceListElements[i]);
			}
		}
		if (targetChecked) {
			console.log("selected checked source = " + target);
			clear_infoArea();
			clear_dataArea();
		}

	});

	ipcRenderer.on('newDevice', function (event, arg) {
		// Request list of devices
		let device_list = ipcRenderer.sendSync('deviceList');
		console.log("Device: ", device_list);
		let deviceListHolder = document.getElementById('deviceListHolder');

		var deviceCounter = 0;
		for ( const key of Object.keys(device_list) ) {
			//console.log(key, " : ", device_list[key]);
			let input = document.createElement('input');
			input.type = "radio";
			input.id = "radio" + deviceCounter.toString();
			input.className = "radio";
			input.name = "deviceList";
			input.value = deviceCounter;
			input.setAttribute("source", key);
			let label = document.createElement('label');
			label.id = "radio" + deviceCounter.toString();
			label.for = "radio" + deviceCounter.toString();
			label.className = "radio";
			label.innerText = device_list[key];
			label.setAttribute("source", key);
			deviceListHolder.appendChild(input);
			deviceListHolder.appendChild(label);
			label.addEventListener('click', function () {

				// First, set associated radio to 'checked'
				document.getElementById(this.for).checked = true;

				// Retrieve & display info about selected device
				let info = ipcRenderer.sendSync('deviceInfo', this.getAttribute('source'));
				const info_keys = Object.keys(info)

				clear_dataArea();
				clear_infoArea();
				let infoArea = document.getElementById('infoArea');
				infoArea.value = info_keys[0] + " : " + info[info_keys[0]];
				for (var i=1;i<info_keys.length;i++) {
					infoArea.value = infoArea.value + '\n' + info_keys[i] + " : " + info[info_keys[i]];
				}

			});

			deviceCounter += 1;

		};
	});


	// "Device info" button
	var infoButton = document.getElementById('infoButton');
	infoButton.onclick = function () {
		// Only identify a selected device
		document.getElementsByName("deviceList").forEach(el => {
			if ( el.checked ) {
				ipcRenderer.send('identify', el.getAttribute('source'));
			}
		});
	}

	// "Read data" button
	var readDataButton = document.getElementById('readDataButton');
	if (!readDataButton) { return; }
	readDataButton.onclick = function () {
		// Toggle value but only react if a device is selected 
		document.getElementsByName("deviceList").forEach(el => {
			if ( el.checked ) {
				if ( this.value == "off") {
					this.value = "on";
					this.innerHTML = "Reading ...";
					document.getElementById('dataArea').setAttribute('isEnabled', true);
				} else {
					this.value = "off";
					this.innerHTML = "Read data";
					document.getElementById('dataArea').setAttribute('isEnabled', false);
				}
			}
		});
	}

	// "Clear" button
	var clearData = document.getElementById('clearDataButton');
	if (!clearData) { return; }
	clearData.onclick = function () {
		document.getElementById('dataArea').value = '';
	}


	// Data area, where device events are displayed (provided 'isEnabled' has been set)
	document.getElementById('dataArea').setAttribute('isEnabled', false);

	/*
		Display event notifications from the device(s)

		The 'detail' of the event is an array containing:
		source    (the device path)
		eventText (previously formatted text describing the event)
	*/
	ipcRenderer.on('deviceEvent', function (event, detail) {
		//console.log("deviceEvent ", detail);
		const source = detail[0];
		const eventText = detail[1];
		//console.log("Event ", eventText, " from: ", source);

		// Are we ready to display data (has 'Read data' button been pressed)?
		var dataArea = document.getElementById('dataArea');
		if (dataArea.getAttribute('isEnabled') == 'false') {
			console.log("dataArea not currently enabled to display data");
			return;
		}

		// Only display data if it's from the selected device
		document.getElementsByName("deviceList").forEach(el => {
			if ( el.checked ) {
				//console.log(el.getAttribute('source'), " is selected");
				if (dataArea.value.length == 0) {
					dataArea.value = eventText;
				} else {
					dataArea.value = dataArea.value + "\n" + eventText;
				}
				dataArea.scrollTop = dataArea.scrollHeight;
			}
		});
	});



	clear_deviceList = function () {
		let deviceListHolder = document.getElementById('deviceListHolder');

		var last;
		while ((last = deviceListHolder.lastChild)) deviceListHolder.removeChild(last);
	};
	clear_infoArea = function () {
		document.getElementById('infoArea').value = '';
	}
	clear_dataArea = function () {
		document.getElementById('dataArea').value = '';

		// Ensure "Read data" button is off
		var readDataButton = document.getElementById('readDataButton');
		readDataButton.value = "off";
		readDataButton.innerHTML = "Read data";
		document.getElementById('dataArea').setAttribute('isEnabled', false);
	}

});



